package com.example.admin.voip.activities;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.admin.voip.DatabaseAdapter;
import com.example.admin.voip.R;
import com.example.admin.voip.activities.LoginActivity;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class RegistrationActivity extends AppCompatActivity implements View.OnClickListener {
    EditText txt_user_name, txt_password;
    Button btn_register;

    private DatabaseAdapter dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        txt_user_name = findViewById(R.id.txt_user_name);
        txt_password = findViewById(R.id.txt_password);
        btn_register = findViewById(R.id.btn_login);
        dbHelper = new DatabaseAdapter(this);
        dbHelper.open();
        btn_register.setOnClickListener(this);
        initControls();



    }


    private void initControls() {
        //Set the activity layout.
        txt_user_name = findViewById(R.id.txt_user_name);
        txt_password = findViewById(R.id.txt_password);
        btn_register = findViewById(R.id.btn_login);


        //Create touch listeners for all buttons.


        btn_register.setOnClickListener(this::RegisterMe);


    }

    private void RegisterMe(View v) {
        String username = txt_user_name.getText().toString();
        String password = txt_password.getText().toString();
        //Encrypt password with MD5.
        password = md5(password);

        //Check database for existing users.
        Cursor user = dbHelper.fetchUser(username, password);
        if (user == null) {
            Toast.makeText(getApplicationContext(), "Database query error",
                    Toast.LENGTH_SHORT).show();
        } else {
            startManagingCursor(user);

            //Check for duplicate usernames
            if (user.getCount() > 0) {
                Toast.makeText(getApplicationContext(), "The username is already registered",
                        Toast.LENGTH_SHORT).show();
                stopManagingCursor(user);
                user.close();
                return;
            }
            stopManagingCursor(user);
            user.close();
            user = dbHelper.fetchUser(username, password);
            if (user == null) {
                Toast.makeText(getApplicationContext(), "Database query error",
                        Toast.LENGTH_SHORT).show();
                return;
            } else {
                startManagingCursor(user);

                if (user.getCount() > 0) {
                    Toast.makeText(getApplicationContext(), "The username is already registered",
                            Toast.LENGTH_SHORT).show();
                    stopManagingCursor(user);
                    user.close();
                    return;
                }
                stopManagingCursor(user);
                user.close();
            }
            //Create the new username.
            long id = dbHelper.createUser(username, password);
            if (id > 0) {
                Toast.makeText(getApplicationContext(), "Your username was created",
                        Toast.LENGTH_SHORT).show();
                //    saveLoggedInUId(id, username, e1.getText().toString());
                Intent i = new Intent(v.getContext(), LoginActivity.class);
                startActivity(i);

                finish();
            } else {
                Toast.makeText(getApplicationContext(), "Failt to create new username",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }


    private String md5(String s) {
        try {

            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(s.getBytes());
            byte[] messageDigest = digest.digest();


            StringBuffer hexString = new StringBuffer();
            for(int i = 0; i < messageDigest.length; i++)
                hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            return s;
        }
    }


    @Override
    public void onClick(View v) {
        String thisUsername = txt_user_name.getText().toString();
        String thisPassword = txt_password.getText().toString();
        dbHelper.createUser(thisUsername, thisPassword);


    }
}
