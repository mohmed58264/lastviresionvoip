package com.example.admin.voip.activities;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.admin.voip.DatabaseAdapter;
import com.example.admin.voip.R;
import com.google.gson.Gson;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    EditText txt_user_name, txt_password;
    String ids;
    int id;
    private DatabaseAdapter dbHelper;
    Button btn_login, btn_register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        try {
            SQLiteDatabase db = openOrCreateDatabase("login", MODE_PRIVATE, null);
            Cursor c = db.rawQuery("select * from user", null);
            c.moveToFirst();
            ids = c.getString(c.getColumnIndex("username"));


        } catch (Exception e) {
            Toast.makeText(getBaseContext(), "" + e, Toast.LENGTH_LONG).show();

        }


        txt_user_name = findViewById(R.id.txt_user_name);
        txt_password = findViewById(R.id.txt_password);
        btn_login = findViewById(R.id.btn_login);    //login
        btn_register = findViewById(R.id.btn_register);   //signup

        dbHelper = new DatabaseAdapter(this);
        dbHelper.open();
        initControls();

    }

    private void initControls() {
        txt_user_name = findViewById(R.id.txt_user_name);
        txt_password = findViewById(R.id.txt_password);
        btn_login = findViewById(R.id.btn_login);
        btn_register = findViewById(R.id.btn_register);
        btn_register.setOnClickListener(this);

        txt_user_name.setText("محمد");
        txt_password.setText("123");
        btn_login.setOnClickListener(v -> LogMeIn());
    }

    private void Exit() {
        finish();
    }


    private void LogMeIn() {
        //Get the username and password
        String thisUsername = txt_user_name.getText().toString();
        String thisPassword = txt_password.getText().toString();

        //Assign the hash to the password
        thisPassword = md5(thisPassword);

        // Check the existing user name and password database
        Cursor theUser = dbHelper.fetchUser(thisUsername, thisPassword);
        if (theUser != null) {
            startManagingCursor(theUser);
            if (theUser.getCount() > 0) {
                dbHelper.fetchUser(thisUsername, thisPassword);
                theUser.close();
                Intent i = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(i);
            }

            //Returns appropriate message if no match is made
            else {
                Toast.makeText(getApplicationContext(),
                        "You have entered an incorrect username or password.",
                        Toast.LENGTH_SHORT).show();
            }
            stopManagingCursor(theUser);
            theUser.close();
        } else {
            Toast.makeText(getApplicationContext(),
                    "Database query error",
                    Toast.LENGTH_SHORT).show();
        }
    }


    private String md5(String s) {
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(s.getBytes());
            byte[] messageDigest = digest.digest();

            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++)
                hexString.append(Integer.toHexString(0xFF & messageDigest[i]));

            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            return s;
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_login) {
            LogMeIn();
        }
        if (v.getId() == R.id.btn_register) {
            Intent I = new Intent(this, RegistrationActivity.class);
            startActivity(I);


        }

    }
}


