package com.example.admin.voip.activities;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.admin.voip.ContactManager;
import com.example.admin.voip.R;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    ArrayList<String> mydata = new ArrayList<String>();
    static final String LOG_TAG = "GradProj";
    private static final int LISTENER_PORT = 50003;
    private static final int BUF_SIZE = 1024;
    private ContactManager contactManager;
    private String displayName;
    private boolean STARTED = false;
    private boolean IN_CALL = false;
    private boolean LISTEN = false;
    ListView list_contacts;
    public final static String EXTRA_CONTACT = "hw.dt83.GradProj.CONTACT";
    public final static String EXTRA_IP = "hw.dt83.GradProj.IP";
    public final static String EXTRA_DISPLAYNAME = "com.example.rzzoozzyy.gradproj";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        list_contacts = findViewById(R.id.list_contacts);
        Log.i(LOG_TAG, "GradProj started");
        Log.i(LOG_TAG, "Start button pressed");
        STARTED = true;
        SQLiteDatabase db = openOrCreateDatabase("login", MODE_PRIVATE, null);
        Cursor c = db.rawQuery("select username from user ", null);
        c.moveToFirst();
        displayName = c.getString(c.getColumnIndex("username"));
        contactManager = new ContactManager(displayName, getBroadcastIp());
        startCallListener();
        // UPDATE BUTTON
        // Updates the list of reachable devices
        final SwipeRefreshLayout swipeRefreshLayout = findViewById(R.id.refresh);
        swipeRefreshLayout.setOnRefreshListener(() -> {
            updateContactList();
            swipeRefreshLayout.setRefreshing(false);
        });
        // CALL BUTTON
        // Attempts to initiate an audio chat session with the selected device
        list_contacts.setOnHoverListener((v, event) -> {
            Toast.makeText(MainActivity.this, "jjj", Toast.LENGTH_SHORT).show();
            return true;
        });
        list_contacts.setOnItemClickListener((parent, view, position, id) -> {
            // Collect details about the selected contact
            String contact = mydata.get(position);
            InetAddress ip = contactManager.getContacts().get(contact);
            IN_CALL = true;
            // Send this information to the MakeCallActivity and start that activity
            Intent intent = new Intent(MainActivity.this, MakeCallActivity.class);
            intent.putExtra(EXTRA_CONTACT, contact);
            String address = ip.toString();
            address = address.substring(1);
            intent.putExtra(EXTRA_IP, address);
            intent.putExtra(EXTRA_DISPLAYNAME, displayName);
            startActivity(intent);
        });
    }

    private void updateContactList() {
        // Create a copy of the HashMap used by the ContactManager
        HashMap<String, InetAddress> contacts = contactManager.getContacts();
        mydata.clear();
        for (String name : contacts.keySet()) {
            mydata.add(name);
            list_contacts.setAdapter(new allContact());
        }
    }

    private InetAddress getBroadcastIp() {
        try {

            WifiManager wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            int ipAddress = wifiInfo.getIpAddress();
            String addressString = toBroadcastIp(ipAddress);


            InetAddress broadcastAddress = InetAddress.getByName(addressString);
            Toast.makeText(this, broadcastAddress.toString(), Toast.LENGTH_SHORT).show();

            return broadcastAddress;
        } catch (UnknownHostException e) {

            Log.e(LOG_TAG, "UnknownHostException in getBroadcastIP: " + e);
            return null;
        }

    }

    private String toBroadcastIp(int ip) {
        // Returns converts an IP address in int format to a formatted string
        return (ip & 0xFF) + "." + ((ip >> 8) & 0xFF) + "." + ((ip >> 16) & 0xFF) + "." + "255";
    }

    private void startCallListener() {
        // Creates the listener thread
        LISTEN = true;
        Thread listener = new Thread(() -> {

            try {
                // Set up the socket and packet to receive
                Log.i(LOG_TAG, "Incoming call listener started");
                DatagramSocket socket = new DatagramSocket(LISTENER_PORT);
                socket.setSoTimeout(1000);
                byte[] buffer = new byte[BUF_SIZE];
                DatagramPacket packet = new DatagramPacket(buffer, BUF_SIZE);
                while (LISTEN) {
                    // Listen for incoming call requests
                    try {
                        Log.i(LOG_TAG, "Listening for incoming calls");
                        socket.receive(packet);
                        String data = new String(buffer, 0, packet.getLength());
                        Log.i(LOG_TAG, "Packet received from " + packet.getAddress() + " with contents: " + data);
                        String action = data.substring(0, 4);
                        if (action.equals("CAL:")) {
                            // Received a call request. Start the ReceiveCallActivity
                            String address = packet.getAddress().toString();
                            String name = data.substring(4, packet.getLength());

                            Intent intent = new Intent(MainActivity.this, ReceiveCallActivity.class);
                            intent.putExtra(EXTRA_CONTACT, name);
                            intent.putExtra(EXTRA_IP, address.substring(1));
                            IN_CALL = true;
                            //LISTEN = false;
                            //stopCallListener();
                            startActivity(intent);
                        } else {
                            // Received an invalid request
                            Log.w(LOG_TAG, packet.getAddress() + " sent invalid message: " + data);
                        }
                    } catch (Exception e) {
                    }
                }
                Log.i(LOG_TAG, "Call Listener ending");
                socket.disconnect();
                socket.close();
            } catch (SocketException e) {

                Log.e(LOG_TAG, "SocketException in listener " + e);
            }
        });
        listener.start();
    }

    private void stopCallListener() {
        // Ends the listener thread
        LISTEN = false;
    }

    @Override
    public void onPause() {

        super.onPause();
        if (STARTED) {

            contactManager.bye(displayName);
            contactManager.stopBroadcasting();
            contactManager.stopListening();
            //STARTED = false;
        }
        stopCallListener();
        Log.i(LOG_TAG, "App paused!");
    }

    @Override
    public void onStop() {

        super.onStop();
        Log.i(LOG_TAG, "App stopped!");
        stopCallListener();
        if (!IN_CALL) {

            finish();
        }
    }

    @Override
    public void onRestart() {

        super.onRestart();
        Log.i(LOG_TAG, "App restarted!");
        IN_CALL = false;
        STARTED = true;
        contactManager = new ContactManager(displayName, getBroadcastIp());
        startCallListener();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class allContact extends BaseAdapter {
        @Override
        public int getCount() {
            return mydata.size();
        }

        @Override
        public Object getItem(int position) {
            return mydata.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = getLayoutInflater();
            View row = inflater.inflate(R.layout.contact_row, parent, false);
            TextView textView = row.findViewById(R.id.textView);
            String s = mydata.get(position);
            textView.setText(s);
            return row;
        }
    }
}
